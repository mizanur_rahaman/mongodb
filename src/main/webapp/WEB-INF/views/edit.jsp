<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="org.bson.Document" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MongoDB</title>
<link rel="stylesheet" href="static/css/bootstrap.css">
<script src="static/js/bootstrap.js"></script>
</head>
<body>
	<center>
		<h2>Edit</h2>
		<p>${message}</p><br>
		<div>
		<form action="<%=request.getContextPath().toString()%>/Update" method="post" name="editForm">
		<%
		Document user = (Document)request.getSession().getAttribute("user");
		%>
		 <input type="hidden" name = "id" value = "<%=user.get("_id")%>"><br><br>
		 <label>Email:</label>&nbsp&nbsp<input type="text" name="email" value = "<%=user.get("email")%>"><br><br>
		 <label>Name:</label>&nbsp&nbsp<input type="text" name="name" value = "<%=user.get("name")%>"><br><br>
		 <input type="submit" value="submit">
		 </form>
		</div>

	</center>
</body>
</html>