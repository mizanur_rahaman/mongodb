<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.mongodb.client.MongoCursor,org.bson.Document" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MongoDB</title>
<link rel="stylesheet" href="static/css/bootstrap.css">
<script src="static/js/bootstrap.js"></script>
</head>
<body>
	<center>
		<h2>${message}</h2>
		<h2>
			 ${name}
		</h2>
	 <table border="1">
	 <thead>
	 <tr><th>Email</th><th>Name</th><th>Delete</th></tr>
	 </thead>
	 <tbody>
	  <%
	  MongoCursor<Document> userList = (MongoCursor<Document>)request.getSession().getAttribute("userList");
	  
	  while(userList.hasNext())
	  {
		  Document doc = userList.next();
	  %>
	  <tr>
	   <td><%=doc.get("email") %></td>
	   <td><%=doc.get("name") %></td>
	   <td><a href="<%=request.getContextPath().toString()%>/Edit?id=<%=doc.get("_id") %>">Edit</a></td>
	   <td><a href="<%=request.getContextPath().toString()%>/Delete?id=<%=doc.get("_id") %>">Delete</a></td>
	  </tr>
	  <%	  
	  }
	  %>
	  
	 </tbody>
	 </table>
	</center>
</body>
</html>