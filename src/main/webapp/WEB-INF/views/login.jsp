<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MongoDB</title>
<link rel="stylesheet" href="static/css/bootstrap.css">
<script src="static/js/bootstrap.js"></script>
</head>
<body>
	<center>
		<h2>Login</h2>
		<p>${message}</p><br>
		<div>
		<form action="<%=request.getContextPath().toString()%>/Login" method="post" name="loginForm">
		 <label>Email:</label>&nbsp&nbsp<input type="text" name="email"><br><br>
		 <label>Password:</label>&nbsp&nbsp<input type="password" name="password"><br><br>
		 <input type="submit" value="submit">
		 </form>
		</div>
		<div>
		<br>
		Don't have an account, Please <a href="<%=request.getContextPath().toString() %>/Registration" >Sign up</a>
		</div>
	</center>
</body>
</html>