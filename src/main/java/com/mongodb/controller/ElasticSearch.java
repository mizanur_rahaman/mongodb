package com.mongodb.controller;

import java.net.InetAddress;
import java.util.Map;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

public class ElasticSearch {
	
	 public static TransportClient client = null;
	 
	 public static void main(String[] args) {
	 
		 try 
		 {
			 
			 ElasticSearch elasticSearch = new ElasticSearch();
			 
			 //elastic connection creation
			 Settings settings = Settings.builder().put("cluster.name", "elasticsearch").build();
			 client = new PreBuiltTransportClient(settings)
			           .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
			 System.out.println("success "+client);
			 
			 // insert document
			 String json = "{\"name\": \"nahid\", \"email\": \"hahid@gmail.com\",\"Age\": 21}";
			 //IndexResponse response = client.prepareIndex("student", "std","1").setSource(json).get();
			
			 
			 SearchResponse response = client.prepareSearch("student")
	                 .setTypes("std")
	                 .setSearchType(SearchType.QUERY_AND_FETCH)
	                 .setFrom(0).setSize(10).setExplain(true)
	                 .execute()
	                 .actionGet();
			 
			SearchHit[] results = response.getHits().getHits();
			System.out.println("Current results: " + results.length);
			for (SearchHit hit : results) 
			{
				System.out.println("------------------------------");
				Map<String,Object> result = hit.getSource();   
				System.out.println(hit.getId());
				System.out.println(result);
			}
			
			 
			 // search by id 
			 //elasticSearch.getDocumentById("student", "std", "2");
			 
			 //search using range query
			 QueryBuilder query =  QueryBuilders.rangeQuery("Age").from(5).to(28);
			 //elasticSearch.getDocumentsBySearchQuery("student", "std", query);
			 
			 query = QueryBuilders.prefixQuery("email","ha");
			// elasticSearch.getDocumentsBySearchQuery("student", "std", query);
			 
			 elasticSearch.getDocumentsByMultiSearchQuery("student", "std");
			 
			 //update document 
			 //elasticSearch.updateDocument("","","","");
			 
			 //delete by id
			 //elasticSearch.deleteDocument("student", "std","1");
			 
			
 
		 } 
		 catch (Exception e)
		 {
			 e.printStackTrace();
		 }
	 }
	 
	 public void insertDocument(String index, String type, String jsonObject, String id)
	 {
		 IndexResponse response = client.prepareIndex(index, type,id).setSource(jsonObject).get();
	 }
	 
	 public void updateDocument(String index, String type, String jsonObject, String id)
	 {
		 UpdateRequest updateRequest = new UpdateRequest("student", "std", "3")
			        .doc("{\"name\":\"hasan\",\"Age\":24}");
			try {
				client.update(updateRequest).get();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	 }
	 
	 public void deleteDocument( String index, String type, String id)
	 {
		 DeleteResponse response = client.prepareDelete(index, type,id).get();
	 }
	 
	 public void getDocumentById(String index, String type, String id)
	 {
		 GetResponse getResponse = client.prepareGet(index,type,id).get();
		 System.out.println(getResponse);
		 Map<String, Object> document =  getResponse.getSource();
		 System.out.println(document);
	 }
	 
	 public void getDocumentsBySearchQuery(String index, String type, QueryBuilder query)
	 {
		 QueryBuilder ageQuery =  QueryBuilders.rangeQuery("Age").from(5).to(28);
		 SearchResponse response = client.prepareSearch(index)
                 .setTypes(type)
                 .setSearchType(SearchType.QUERY_AND_FETCH)
                 .setQuery(query)
                 .setQuery(ageQuery)
                 .setFrom(0).setSize(10).setExplain(true)
                 .execute()
                 .actionGet();
		 
		SearchHit[] results = response.getHits().getHits();
		System.out.println("Current results: " + results.length);
		for (SearchHit hit : results) 
		{
			System.out.println("------------------------------");
			Map<String,Object> result = hit.getSource();   
			System.out.println(result);
		}
		
		//MultiSearchRequest request = response = client.multiSearch(request);
		
	 }
	 
	 public void getDocumentsByMultiSearchQuery(String index, String type)
	 {
		 System.out.println("-----------multi search-----------");
	
		 QueryBuilder boolQuery = QueryBuilders.boolQuery()
				    .must(QueryBuilders.prefixQuery("email", "ha"))    
				    .must(QueryBuilders.rangeQuery("Age").from("5").to("24")); 

		 SearchResponse response = client.prepareSearch(index)
                 .setTypes(type)
                 .setSearchType(SearchType.QUERY_AND_FETCH)
                 .setQuery(boolQuery)
                 .setFrom(0).setSize(10).setExplain(true)
                 .execute()
                 .actionGet();
		 
		SearchHit[] results = response.getHits().getHits();
		System.out.println("Current results: " + results.length);
		for (SearchHit hit : results) 
		{
			System.out.println("------------------------------");
			Map<String,Object> result = hit.getSource();   
			System.out.println(result);
		}
	 }
}
