package com.mongodb.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

@Controller
public class LoginController {
	String message = "Hello welcome to dream world. Most beautiful and worst place in galaxy";
 
	@RequestMapping("/")
	public ModelAndView login() { 
		ModelAndView mv = new ModelAndView("login");
		mv.addObject("message","");
		return mv;
	}
	
	@RequestMapping("/Login")
	public ModelAndView login1() { 
		ModelAndView mv = new ModelAndView("login");
		mv.addObject("message","");
		return mv;
	}
	
	@RequestMapping(value="/Login",method=RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response) {
		
		MongoClient mongoClient = null;
		try 
		{
			mongoClient = new MongoClient( "localhost" , 27017 );
			MongoDatabase database = mongoClient.getDatabase("mongodb");
			
		    BasicDBObject query = new BasicDBObject();
		    query.put("email", request.getParameter("email"));
		    query.put("password", request.getParameter("password"));
		    
		    Document document  = database.getCollection("user").find(query).first();
		     
		    if(document == null)
			{
		    	ModelAndView mv= new ModelAndView("login");
		    	 mv.addObject("message","Invalid email or password");
		    	 return mv;
			}       
				
	        MongoCursor<Document> userList = database.getCollection("user").find().iterator();
	        
	        ModelAndView mv = new ModelAndView("index");
		    mv.addObject("message", "MongoDB Test Project.");
			mv.addObject("name", "Welcome "+document.get("name"));
			request.getSession().setAttribute("userList", userList);
			return mv;
		}
		catch(Exception ex){}
		finally 
		{
			if(mongoClient!=null)
				mongoClient.close();
		}
		return new ModelAndView("login");
	}
	
	@RequestMapping("/Registration")
	public ModelAndView getRegistrationForm() { 
		ModelAndView mv = new ModelAndView("signup");
		mv.addObject("message","");
		return mv;
	}
	
	@RequestMapping(value="/Registration",method=RequestMethod.POST)
	public ModelAndView registration(HttpServletRequest request,HttpServletResponse response ) {
		
		if(request.getParameter("email") == null || request.getParameter("email").trim().length() == 0
		  || request.getParameter("password") == null || request.getParameter("password").trim().length() == 0
		  || request.getParameter("name") == null || request.getParameter("name").trim().length() == 0)
		{
			ModelAndView mv = new ModelAndView("signup");
			mv.addObject("message","Please enter valid entry");
			return mv;
		}
		
		MongoClient mongoClient = null;
		try
		{
			mongoClient = new MongoClient( "localhost" , 27017 ); 
		    MongoDatabase db = mongoClient.getDatabase("mongodb");
		    MongoCollection<Document> user = db.getCollection("user");
		    
		    Document doc = new Document("email", request.getParameter("email")).
		            append("password", request.getParameter("password")).
		            append("name", request.getParameter("name"));
		    
			user.insertOne(doc);
				
	        MongoCursor<Document> userList = user.find().iterator();
	  
			request.getSession().setAttribute("userList", userList);

		    ModelAndView mv = new ModelAndView("index");
		    mv.addObject("message", "MongoDB Test Project.");
			mv.addObject("name", "Added user successfully");
			return mv;
		}
		catch(Exception ex){}
		finally{
			if(mongoClient!=null)
				mongoClient.close();
		}
		
		return new ModelAndView("signup");
	}

	@RequestMapping("/Edit")
	public ModelAndView getEditForm(HttpServletRequest request,HttpServletResponse response) { 
		MongoClient mongoClient = null;
		try{
			mongoClient = new MongoClient( "localhost" , 27017 ); 
			
		    MongoDatabase db = mongoClient.getDatabase("mongodb");
		   
		    BasicDBObject query = new BasicDBObject();
		    query.put("_id", new ObjectId(request.getParameter("id")));
		    
		    Document document  = db.getCollection("user").find(query).first();

		    if(document == null)
			{
		    	 return new ModelAndView("login");
			}
	    
			request.getSession().setAttribute("user", document);
		    ModelAndView mv = new ModelAndView("edit");
		    mv.addObject("message", "");
			return mv;
		}
		catch(Exception ex){}
		finally{
			if(mongoClient!=null)
				mongoClient.close();
		}
		return new ModelAndView("login");
	}
	
	@RequestMapping(value="/Update",method=RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request,HttpServletResponse response ) {
		
		if(request.getParameter("email") == null || request.getParameter("email").trim().length() == 0
				  || request.getParameter("id") == null || request.getParameter("id").trim().length() == 0
				  || request.getParameter("name") == null || request.getParameter("name").trim().length() == 0)
		{
			ModelAndView mv = new ModelAndView("edit");
			mv.addObject("message","Please enter valid entry");
			return mv;
		}
		
		MongoClient mongoClient = null;
		try{
			mongoClient = new MongoClient( "localhost" , 27017 ); 
			
		    MongoDatabase db = mongoClient.getDatabase("mongodb");
		    MongoCollection<Document> user = db.getCollection("user");
		    
		    Document doc = new Document("$set",new Document("email", request.getParameter("email")).
		            	 append("name", request.getParameter("name")));
		    
		    user.updateOne(new Document("_id", new ObjectId(request.getParameter("id"))),doc);
		   
	        MongoCursor<Document> userList = user.find().iterator();
	    
			request.getSession().setAttribute("userList", userList);
			
		    ModelAndView mv = new ModelAndView("index");
		    mv.addObject("message", "MongoDB Test Project.");
			mv.addObject("name", "Updated user successfully");
			return mv;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			if(mongoClient!=null)
				mongoClient.close();
		}
		return new ModelAndView("edit");
	}
	
	@RequestMapping(value="/Delete",method=RequestMethod.GET)
	public ModelAndView delete(HttpServletRequest request,HttpServletResponse response ) {
				
		MongoClient mongoClient = null;
		try{
			mongoClient = new MongoClient( "localhost" , 27017 ); 
			
		    MongoDatabase db = mongoClient.getDatabase("mongodb");
		    MongoCollection<Document> user = db.getCollection("user");
		    
		    if(request.getParameter("id") != null || request.getParameter("id").trim().length() > 0)
			{
		    	user.deleteOne(new BasicDBObject("_id", new ObjectId(request.getParameter("id"))));
			}
		    
	        MongoCursor<Document> userList = user.find().iterator();
	    
			request.getSession().setAttribute("userList", userList);
			
		    ModelAndView mv = new ModelAndView("index");
		    mv.addObject("message", "MongoDB Test Project.");
			mv.addObject("name", "Deleted user successfully");
			return mv;
		}
		catch(Exception ex){}
		finally{
			if(mongoClient!=null)
				mongoClient.close();
		}
		return new ModelAndView("login");
	}
}
