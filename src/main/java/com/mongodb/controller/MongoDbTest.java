package com.mongodb.controller;

import java.util.Iterator;

import javax.swing.text.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDbTest {

	public static void main(String[] args) {
		 // Creating a Mongo client 
	      MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
	   
	      // Creating Credentials 
	      MongoCredential credential; 
	      credential = MongoCredential.createCredential("mongo", "mongodb", 
	         "password".toCharArray()); 
	      System.out.println("Connected to the database successfully");  
	      
	      // Accessing the database 
	      MongoDatabase database = mongo.getDatabase("mongodb");
	      MongoCollection<org.bson.Document> collection = database.getCollection("user");
	      System.out.println("Collection sampleCollection selected successfully"); 

	      // Getting the iterable object 
	      FindIterable<org.bson.Document> iterDoc = collection.find(); 
	      int i = 1; 

	      // Getting the iterator 
	      Iterator it = iterDoc.iterator(); 
	    
	      while (it.hasNext()) {  
	         System.out.println(it.next());  
	      i++; 
	      }
	      System.out.println("Credentials ::"+ credential);

	}

}
